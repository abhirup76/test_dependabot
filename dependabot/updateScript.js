const { spawn } = require('child_process');


const runDependabotScript = async (repoPath,access_token,package_manager,directory) => {
    const args = {
        PROJECT_PATH : repoPath,
        GITLAB_ACCESS_TOKEN : access_token,
        PACKAGE_MANAGER : package_manager, 
        DIRECTORY_PATH : directory
    };

    const rubyScriptPath = './dependabot-script/index.rb';
    const rubyProcess = spawn('ruby', [rubyScriptPath, JSON.stringify(args)],{
        env: { ...process.env, LANG: 'en_US.UTF-8', LC_ALL: 'en_US.UTF-8' }
    });

    rubyProcess.stdout.on('data', (data) => {
        console.log(data.toString());
    });

    rubyProcess.stderr.on('data', (data) => {
        console.log(data.toString());
    });

    rubyProcess.on('close', (code) => {
        console.log(`Process exited with code ${code}`);
    });
}

args = process.argv
runDependabotScript(args[2],args[3],args[4],args[5])