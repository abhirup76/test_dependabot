const { exec } = require('child_process');

//function to execute a command in the command line

const runCommand = (command) => {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                reject({ error, stderr });
                console.log(error);
            } else {
                resolve(stdout);
            }
        });
    });
};

module.exports = {runCommand};